// Example express application adding the parse-server module to expose Parse
// compatible API routes.
require('dotenv').config();
var express = require('express');
var ParseServer = require('parse-server').ParseServer;
var path = require('path');
const sharp = require('sharp');

var databaseUri = process.env.DB_URI;
if (!databaseUri) {
  console.log('DATABASE_URI not specified, falling back to localhost.');
}

var api = new ParseServer({
  databaseURI: databaseUri || 'mongodb://localhost:27017/dev',
  cloud: process.env.CLOUD_CODE_MAIN || __dirname + '/cloud/main.js',
  appId: process.env.APP_ID,
  masterKey: process.env.MASTER_KEY, //Add your master key here. Keep it secret!
  serverURL: process.env.SERVER_URL,  // Don't forget to change to https if needed
  liveQuery: {
    classNames: ["Posts", "Comments"] // List of classes to support for query subscriptions
  }
});
// Client-keys like the javascript key or the .NET key are not necessary with parse-server
// If you wish you require them, you can set them as options in the initialization above:
// javascriptKey, restAPIKey, dotNetKey, clientKey

var app = express();

// Serve static assets from the /public folder
app.use('/public', express.static(path.join(__dirname, '/public')));

// Serve the Parse API on the /parse URL prefix
var mountPath = process.env.PARSE_MOUNT || '/parse';
app.use(mountPath, api);

// Parse Server plays nicely with the rest of your web routes
app.get('/', function (req, res) {
  res.status(200).send('I dream of being a website.  Please star the parse-server repo on GitHub!');
});

// There will be a test page available on the /test path of your server url
// Remove this before launching your app
app.get('/test', function (req, res) {
  res.sendFile(path.join(__dirname, '/public/test.html'));
});

var port = process.env.PORT;
var httpServer = require('http').createServer(app);
httpServer.listen(port, function () {
  console.log('parse-server-example running on port ' + port + '.');
});

// This will enable the Live Query real-time server
ParseServer.createLiveQueryServer(httpServer);

var fs = require('fs'),
  request = require('request');

var download = function (uri, filename, callback) {
  request.head(uri, function (err, res, body) {
    console.log('content-type:', res.headers['content-type']);
    console.log('content-length:', res.headers['content-length']);

    request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
  });
};

app.get('/resize', function (req, res) {

  const myJSON = JSON.stringify(req.query);
  const obj = JSON.parse(myJSON);
  const order = parseInt(obj.searchKey);

  res.status(200).send('Resize!');
  Parse.serverURL = process.env.SERVER_URL;
  Parse.initialize(process.env.APP_ID);
  const DubaiLandmarks = Parse.Object.extend('DubaiLandmarks');
  const query = new Parse.Query(DubaiLandmarks);
  query.equalTo('order', order);
  query.find().then((data) => {
    download(data[0].attributes.photo._url, data[0].attributes.photo._name, function () {
      const tempName = data[0].attributes.photo._name;

      sharp(tempName)
        .resize({
          width: parseInt(process.env.PHOTO_WIDTH),
          height: parseInt(process.env.PHOTO_HEIGHT),
          fit: sharp.fit.inside,
          position: sharp.strategy.entropy
        })
        .toFile('thumb' + tempName).then(() => {
          fs.readFile('thumb' + tempName, (err, data2) => {
            //error handle
            if (err) res.status(500).send(err);
            //convert image file to base64-encoded string
            let base64Image = new Buffer(data2, 'binary').toString('base64');
            const parseFile = new Parse.File('thumb_' + data[0].attributes.photo._name, { base64: base64Image });
            data[0].set('photo_thumb', parseFile);
            data[0].save()
          })
        });
      console.log('done');
    })

  });
});
