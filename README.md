# landmarks-backend

## How to run

1.  In root dir run `npm i`.
2.  Install parse-dasboard: `npm install -g parse-dashboard`
3.  Run `npm start`.
4.  Run dashboard: `parse-dashboard --dev --appId yourAppId --masterKey yourMasterKey --serverURL "http://example/parse"`. AppId, MasterKey and serverURL are in .env file
5.  Navigate to `http://localhost:4040/` for dashboard

## Prerequisites

* Nodejs version 12.16
* Parse server 2.7.4